from bs4 import BeautifulSoup
import requests
import pandas as pd
## Getting data for the month of June.
part1="http://www.accuweather.com/en/in/kanpur/206679/migraine-june-weather/206679?monyr="
part2="/1/2016&view=table"
date, ppt=[], []
for month in range(12):
	url=part1+str(month)+part2
	proxies = {
	  'http': 'http://ironport2.iitk.ac.in:3128',
	  'https': 'http://ironport2.iitk.ac.in:3128',
	}
	r = requests.get(url, proxies=proxies)
	soup = BeautifulSoup(r.text, 'lxml')

	## I have the page now.
	table = soup.find_all(class_='calendar-list')
	#date and precipitation arrays
	for row in table[0]('tr')[1:]:
		curr_date = row.find('time').string
		date.append(curr_date)
		rain = row.find_all('td')[1]
		ppt.append(rain.contents[0].string.strip(' '))

#Date extraction complete. Now fix date ranges
#Caution : enetr in mm/dd/yyyy format!
rng = pd.date_range('1/1/2016', periods=len(date), freq='D')
df = pd.Series(ppt, index=rng)
df.to_csv('weather.csv')