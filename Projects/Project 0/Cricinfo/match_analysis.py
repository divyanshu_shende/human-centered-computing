
# coding: utf-8

# In[ ]:

import requests
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup


# In[ ]:

url = 'http://www.espncricinfo.com/ci/engine/match/913617.html'
r = requests.get(url)
soup = BeautifulSoup(r.text, 'lxml')


# In[ ]:

table = soup.find_all(class_='batting-table innings')


# In[ ]:

ts = [[], [], [], []]
lst = []
key = []
for innings in range(len(table)):
    for string in table[innings](class_='th-innings-heading')[0].strings:
        lst.append(str(string))

    key.append(lst[0].strip().replace(' 1st innings', '').replace(' 2nd innings', ''))
    
    for batsman in table[innings]('tr', class_=''):
        ts[innings].append(int(batsman.find(class_='bold').string))
    lst = []    


# In[ ]:

pdict = [[], []]
pdict[0] = [key[0], ts[0]+ts[2]]
pdict[1] = [key[1], ts[1] + ts[3]]
    


# In[ ]:

columns = dict(pdict)

