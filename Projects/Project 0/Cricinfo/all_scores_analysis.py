
# coding: utf-8

# In[ ]:

import requests
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
from time import sleep
import statsmodels.api as sm


# In[ ]:

pd.set_option('display.max_colwidth', -1)
df = pd.read_csv('all_years_data.csv')
dbase = pd.read_csv('all_runs_scored.csv')
tempSeries = dbase['Runs']
tempSeries = dbase['Runs'].sort_values(inplace=0)


# In[ ]:

headers = requests.utils.default_headers()
headers.update(
    {
        'User-Agent': 'Custom User Agent 1.0',
        'From': 'nsrivast@iitk.ac.in'  # Stick in your IITK email id
    }
)


# In[ ]:

scores = []
salience_list = [0]*(len(df)-1)
outcome = [0]*(len(df)-1)


# In[ ]:

for row in range(1061,len(df)-1):
    url = df[row-1:row].url.item()
    team_1 = df[row-1:row].Team_1.item()
    team_2 = df[row-1:row].Team_2.item()
    winner = df[row-1:row].Winner.item()
    sleep(int(np.random.normal(3,0.2)*100)/(100*1.0))
    print(row)
    r = requests.get(url, headers=headers)
    soup = BeautifulSoup(r.text, 'lxml')
    table = soup.find_all(class_='batting-table innings')
    
    lst = []
    key = []
    
    ts = [[], [], [], []]
    salience = [[], [], [], []]
    for innings in range(len(table)):
        for string in table[innings](class_='th-innings-heading')[0].strings:
            lst.append(str(string))
        key.append(lst[0].strip().replace(' 1st innings', '').replace(' 2nd innings', ''))

        for batsman in table[innings]('tr', class_=''):                
            if batsman.find(class_='bold').string.isdigit():                
                ts[innings].append(int(batsman.find(class_='bold').string))                
                salience[innings].append(len(tempSeries)/(tempSeries >= int(batsman.find(class_='bold').string)).sum())        
        lst = []    
    scores.append(ts)
    t1_idx = [i for i, x in enumerate(key) if x == df[row-1:row].Team_1.values[0]]
    t2_idx = [i for i, x in enumerate(key) if x == df[row-1:row].Team_2.values[0]]
    if(len(t1_idx)==2):        
        t1 = salience[t1_idx[0]] + salience[t1_idx[1]]
    elif(len(t1_idx)==1):        
        t1 = salience[t1_idx[0]]
    else:
        t1 = [0]
    if(len(t2_idx)==2):        
        t2 = salience[t2_idx[0]] + salience[t2_idx[1]]
    elif(len(t2_idx)==1):        
        t2 = salience[t2_idx[0]]        
    else:
        t2 = [0]
    t1.sort(reverse=1)
    t2.sort(reverse=1)
    salience_list[row] = np.sum(t1[0:3]) - np.sum(t2[0:3])
    if (winner==team_1):
        outcome[row] = 1
    elif(winner==team_2):
        outcome[row] = -1
    else:
        outcome[row] = 0


# In[ ]:

plt.scatter(salience_list, outcome)
plt.xlabel('Salience score')
plt.ylabel('Win indicator \n(direction of salience score)')
plt.show()


# In[ ]:

results = {'Salience': salience_list, 'Outcome': outcome}
resdf = pd.DataFrame(results)


# In[ ]:

resdf['Predictor'] = (resdf['Salience']>0)*1


# In[ ]:

from scipy.stats import pearsonr
pearsonr(resdf['Outcome'], resdf['Predictor']) # a decent correlation between the outcome and the predictor


# In[ ]:

resdf['intercept'] = 1.0


# In[ ]:

resdf.Outcome[resdf['Outcome']==-1] = 0 # treating draws as losses for the home team. YMMV.


# In[ ]:

logit = sm.Logit(resdf['Outcome'], resdf[['Salience', 'intercept']])
logit_result = logit.fit()


# In[ ]:

print logit_result.summary() # logistic regression with the original salience score gives terrible results. Why?


# In[ ]:



