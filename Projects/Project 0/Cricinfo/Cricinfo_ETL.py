
# coding: utf-8

# In[ ]:

import numpy as np
import pandas as pd
import matplotlib as plt
import requests

from bs4 import BeautifulSoup


# In[ ]:

url = 'http://stats.espncricinfo.com/ci/engine/records/team/match_results.html?class=1;id=2016;type=year'
r = requests.get(url)
soup = BeautifulSoup(r.text, 'lxml')


# In[ ]:

team1 = []
team2 = []
winner = []
margin = []
table = soup.find(class_='engineTable')


# In[ ]:

for row in table.find_all('tr')[1:]:
    col = row.find_all('td')
    col_0 = col[0].find('a').string.strip()
    team1.append(col_0)
    col_1 = col[1].find('a').string.strip()
    team2.append(col_1)
    if(col[2].find('a')):
        col_2 = col[2].find('a').string.strip()
    else:
        col_2 = col[2].string.strip()
    winner.append(col_2)
    if(col[3].string):
        col_3 = col[3].string.strip()
    else:
        col_3 = '0'
    margin.append(col_3)


# In[ ]:

columns = {'Team_1': team1, 'Team_2': team2, 'Winner': winner, 'Margin': margin}


# In[ ]:

df = pd.DataFrame(columns)
df.to_csv('data.csv')

