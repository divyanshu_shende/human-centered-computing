
# coding: utf-8

# In[ ]:

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import requests
import scipy.stats as sts

from bs4 import BeautifulSoup


# In[ ]:

headers = requests.utils.default_headers()
headers.update(
    {
        'User-Agent': 'Custom User Agent 1.0',
        'From': 'nsrivast@iitk.ac.in'  # Stick in your IITK email id
    }
)


# In[ ]:

all_data = {}

team1 = []
team2 = []
winner = []
margin = []
matchid = []
matchurl = []
matchdate = []

pd.set_option('display.max_colwidth', -1)

for date in range(1992,2017):
    url = 'http://stats.espncricinfo.com/ci/engine/records/team/match_results.html?class=1;id=' + str(date) + ';type=year'
    r = requests.get(url, headers=headers)
    soup = BeautifulSoup(r.text, 'lxml')
    table = soup.find(class_='engineTable')

    for row in table.find_all('tr')[1:]:
        col = row.find_all('td')
        col_0 = col[0].find('a').string.strip()
        team1.append(col_0)
        col_1 = col[1].find('a').string.strip()
        team2.append(col_1)
        if(col[2].find('a')):
            col_2 = col[2].find('a').string.strip()
        else:
            col_2 = col[2].string.strip()
        winner.append(col_2)
        if(col[3].string):
            col_3 = col[3].string.strip()
        else:
            col_3 = '0'
        margin.append(col_3)
        col_4 = col[6].find('a').string.strip().replace('Test # ', '')
        matchid.append(col_4)
        col_5 = 'http://stats.espncricinfo.com' + col[6].find('a').get('href')
        matchurl.append(col_5)
        matchdate.append(str(date))

columns = {'Team_1': team1, 'Team_2': team2, 'Winner': winner, 'Margin': margin, 'Game': matchid, 'url': matchurl, 'Year': matchdate}


# In[ ]:

df = pd.DataFrame(columns)


# In[ ]:

TEAM_LIST = np.unique(df['Team_1'])


# In[ ]:

win_rate = []    # can calculate team performance over seasons
win_rate_year = []
for team in TEAM_LIST:
    for year in range(1992,2017):
        if (len(df[(df.Team_1==team) & (df.Year==str(year))]) + len(df[(df.Team_2==team) & (df.Year==str(year))])) > 0:
            denom = len(df[(df.Team_1==team) & (df.Year==str(year))]) + len(df[(df.Team_2==team) & (df.Year==str(year))]) 
            numer = len(df[(df.Team_1==team) & (df.Year==str(year)) & (df.Winner==team)])                    + 0.33*len(df[(df.Team_1==team) & (df.Year==str(year)) & (df.Winner=='drawn')])                    + len(df[(df.Team_2==team) & (df.Year==str(year)) & (df.Winner==team)])                      + 0.67*len(df[(df.Team_2==team) & (df.Year==str(year)) & (df.Winner=='drawn')])
            wr_value = float(numer/(1.0))
            win_rate_year.append(wr_value)
        else:
            win_rate_year.append(0)
    win_rate.append(win_rate_year)
    win_rate_year = []


# In[ ]:

team = TEAM_LIST[3] # can run SQL like queries
year = 2014
df[(df.Team_2==team) & (df.Year==str(year))]


# In[ ]:

from scipy.interpolate import UnivariateSpline
z1 = UnivariateSpline(range(1992,2017),np.transpose(win_rate[0]))
z2 = UnivariateSpline(range(1992,2017),np.transpose(win_rate[3]))


# In[ ]:

plt.figure(figsize=(20,10))
plt.subplot(1,2,1)
plt.plot(range(1992,2017), z1(range(1992,2017)))
plt.xlabel('Years')
plt.ylabel('Wins')
plt.title(TEAM_LIST[0])
plt.subplot(1,2,2)
plt.plot(range(1992,2017), z2(range(1992,2017)))
plt.rcParams['font.size'] = 20
plt.rcParams['lines.linewidth'] = 8
plt.xlabel('Years')
plt.ylabel('Wins')
plt.title(TEAM_LIST[3])
plt.show()

