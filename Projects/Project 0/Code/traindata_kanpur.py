from bs4 import BeautifulSoup
import requests
import pandas as pd

url="http://railtimetable.blogspot.in/2012/07/kanpur-central-cnb-time-table.html"
proxies = {
	'http': 'http://ironport2.iitk.ac.in:3128',
	'https': 'http://ironport2.iitk.ac.in:3128',
}
r = requests.get(url, proxies=proxies, verify=False)
soup = BeautifulSoup(r.text, "lxml")

hierarchy=['Rajdhani', 'Shatabdi', 'Duronto']
tr_num, tr_type, tr_source, tr_dest =[], [], [], []
non_spl_tr = []
## I have the page now.
# table = soup.find_all(class_='srhres')
table = soup.find_all('table')
for t in table:
	for row in t('tr')[1:]:
		tr_num.append((row.find_all('td'))[0].string)
		tr_type.append((row.find_all('td'))[1].string)
		tr_source.append((row.find_all('td'))[2].string)
		tr_dest.append((row.find_all('td'))[3].string)
		
fd = open('spl_cnb', 'w')
for i in range(len(tr_num)):
	if tr_type[i] in hierarchy:
		to_write=tr_num[i] + "\n"
		fd.write(to_write)
	else:
		non_spl_tr.append(tr_num[i])
fd.close()

#store data in dep_cnb
fd = open('dep_cnb', 'w')
for i in range(len(tr_num)):
	if(tr_type[i] not in hierarchy):
		to_write=tr_num[i] + "\n"
		fd.write(to_write)
	else:
		continue
fd.close()
print("Table data extracted.")

for num in non_spl_tr:
	url = 'http://www.irctclive.in/RunningTrainHistoryStatus/'+str(num)+'/lastyear'
	print("Looking at train " + str(num))
	r = requests.get(url)
	soup = BeautifulSoup(r.text, 'lxml')
	if 'Server Error (500)' in soup.text:
		continue
	table = soup.find_all(class_='table datatable table-striped table-bordered')
	ofname = 'Data/dep_cnb/'+str(num)
	of = open(ofname, 'w')
	for row in table[0]('tr')[1:]:
		col = row('td')
		# date.append(col[0].string)
		# delay.append(col[2].string)
		delay_hrs = str(col[2].string)
		if delay_hrs == '-':
			delay = 0
		else:
			split_delay = delay_hrs.split(' ')
			split_delay = [d for d in split_delay if d != '']
			if 'Hr' in split_delay[1]:
				delay = int(split_delay[0])*60
				if 'Min' in split_delay[3]:
					delay = delay + int(split_delay[2])
			else:
				if 'Min' in split_delay[1]:
					delay = int(split_delay[0])
		writestr=str(col[0].string)+','+str(delay)+' Mins\n'
		of.write(writestr)
	of.close()