# In[1]:

from bs4 import BeautifulSoup
import requests
import pandas as pd

# In[2]:
flist = ['rajdhanis', 'shatabdis', 'durontos']

# In[3]:
for train in flist:
	fd = open(train, 'r')
	tr_num=fd.readlines()
	fd.close()
	for i in range(len(tr_num)):
		tr_num[i] = tr_num[i].strip('\n')
	for num in tr_num:
		url = 'http://www.irctclive.in/RunningTrainHistoryStatus/'+str(num)+'/lastyear'
		print(url)
		r = requests.get(url)
		soup = BeautifulSoup(r.text, 'lxml')

		table = soup.find_all(class_='table datatable table-striped table-bordered')
		ofpath = 'Data/'+train+'/' + str(num)
		of = open(ofpath, 'w')
		for row in table[0]('tr')[1:]:
		    col = row('td')
		    # date.append(col[0].string)
		    # delay.append(col[2].string)
		    writestr=str(col[0].string)+','+str(col[2].string)+'\n'
		    of.write(writestr)
		of.close()
