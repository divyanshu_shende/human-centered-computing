from bs4 import BeautifulSoup
import requests
import pandas as pd
import os
from scipy import stats
import numpy as np

# Kanpur trains data
delays_cnb = []
mean_delays_cnb = []
path = 'Data/dep_cnb/'
files = os.listdir(path)
for train in files:
	fpath = path + str(train)
	fd = open(fpath, 'r')
	data = fd.read()
	data = data.split('\n')
	data = [x for x in data if x != '']
	fd.close()
	delay = []
	for line in data:
		stuff = line.split(',')
		delay_mins = stuff[-1]
		if delay_mins.endswith(' Mins') == True:
			delay_mins = delay_mins[:-5]
			delay.append(int(delay_mins))
	mean_delays_cnb.append(sum(delay)/len(delay))
	delays_cnb.append(delay)

ofpath = 'Data/kanpur_delays'
of = open(ofpath, 'w')
for delay in delays_cnb:
	of.write(str(delay) + "\n")

# Bhopal trains data
delays_bhopal = []
mean_delays_bhopal = []
path = 'Data/dep_bhopal/'
files = os.listdir(path)
for train in files:
	fpath = path + str(train)
	fd = open(fpath)
	data = fd.read()
	data = data.split('\n')
	data = [x for x in data if x != '']
	fd.close()
	delay = []
	for line in data:
		stuff = line.split(',')
		delay_mins = stuff[-1]
		if delay_mins.endswith(' Mins') == True:
			delay_mins = delay_mins[:-5]
			delay.append(int(delay_mins))
	mean_delays_bhopal.append(sum(delay)/len(delay))
	delays_bhopal.append(delay)

# print(len(mean_delays_cnb))
# print(len(mean_delays_bhopal))
ofpath = 'Data/bhopal_delays'
of = open(ofpath, 'w')
for delay in delays_bhopal:
	of.write(str(delay) + "\n")
	
# Delhi trains data
delays_ndls = []
mean_delays_ndls = []
path = 'Data/dep_ndls/'
files = os.listdir(path)
for train in files:
	fpath = path + str(train)
	fd = open(fpath)
	data = fd.read()
	data = data.split('\n')
	data = [x for x in data if x != '']
	fd.close()
	delay = []
	for line in data:
		stuff = line.split(',')
		delay_mins = stuff[-1]
		if delay_mins.endswith(' Mins') == True:
			delay_mins = delay_mins[:-5]
			delay.append(int(delay_mins))
	mean_delays_ndls.append(sum(delay)/len(delay))
	delays_ndls.append(delay)

# print(len(mean_delays_cnb))
# print(len(mean_delays_ndls))
ofpath = 'Data/ndls_delays'
of = open(ofpath, 'w')
for delay in delays_ndls:
	of.write(str(delay) + "\n")


# Bangalore trains data
delays_blore = []
mean_delays_blore = []
path = 'Data/dep_blore/'
files = os.listdir(path)
for train in files:
	fpath = path + str(train)
	fd = open(fpath, 'r')
	data = fd.read()
	data = data.split('\n')
	data = [x for x in data if x != '']
	fd.close()
	delay = []
	for line in data:
		stuff = line.split(',')
		delay_mins = stuff[-1]
		if delay_mins.endswith(' Mins') == True:
			delay_mins = delay_mins[:-5]
			delay.append(int(delay_mins))
	mean_delays_blore.append(sum(delay)/len(delay))
	delays_blore.append(delay)

ofpath = 'Data/blore_delays'
of = open(ofpath, 'w')
for delay in delays_blore:
	of.write(str(delay) + "\n")


mean_A = sum(mean_delays_cnb)/len(mean_delays_cnb)
mean_B = sum(mean_delays_bhopal)/len(mean_delays_bhopal)
mean_C = sum(mean_delays_ndls)/len(mean_delays_ndls)
mean_C = sum(mean_delays_blore)/len(mean_delays_blore)

print("Kanpur mean = " + str(mean_A))
print("Bhopal mean = " + str(mean_B))
print("New Delhi mean = " + str(mean_C))
print("Bangalore mean = " + str(mean_D))

test1 = stats.ttest_ind(mean_delays_cnb, mean_delays_bhopal, equal_var = False)
test2 = stats.ttest_ind(mean_delays_cnb, mean_delays_ndls, equal_var = False)
test3 = stats.ttest_ind(mean_delays_ndls, mean_delays_bhopal, equal_var = False)
test4 = stats.ttest_ind(mean_delays_cnb, mean_delays_blore, equal_var = False)
test5 = stats.ttest_ind(mean_delays_ndls, mean_delays_blore, equal_var = False)
test6 = stats.ttest_ind(mean_delays_blore, mean_delays_bhopal, equal_var = False)

print("\n\nKanpur-Bhopal t-test:")
print("Statistic = " + str(test1.statistic))
print("P-value = " + str(test1.pvalue))
print("\n\nKanpur-New Delhi t-test:")
print("Statistic = " + str(test2.statistic))
print("P-value = " + str(test2.pvalue))
print("\n\nNew Delhi-Bhopal t-test:")
print("Statistic = " + str(test3.statistic))
print("P-value = " + str(test3.pvalue))
print("\n\nKanpur-Bangalore t-test:")
print("Statistic = " + str(test4.statistic))
print("P-value = " + str(test4.pvalue))
print("\n\nNew Delhi-Bangalore t-test:")
print("Statistic = " + str(test5.statistic))
print("P-value = " + str(test5.pvalue))
print("\n\nBangalore-Bhopal t-test:")
print("Statistic = " + str(test6.statistic))
print("P-value = " + str(test6.pvalue))
print("\n\nKanpur variance = " + str(np.var(mean_delays_cnb)))
print("Bhopal variance = " + str(np.var(mean_delays_bhopal)))
print("NDLS variance = " + str(np.var(mean_delays_ndls)))
print("Bangalore variance = " + str(np.var(mean_delays_blore)))