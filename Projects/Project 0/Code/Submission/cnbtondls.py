from bs4 import BeautifulSoup
import requests
import pandas as pd

# http://www.irctclive.in/TrainBetweenStationStatus/KANPUR%20CENTRAL%20-%20CNB/NEW%20DELHI%20-%20NDLS/01-02/GN
url=[]
for x in range(1,8):
	url.append("http://www.irctclive.in/TrainBetweenStationStatus/KANPUR%20CENTRAL%20-%20CNB/NEW%20DELHI%20-%20NDLS/0"+str(x)+"-02/GN")

proxies = {
	'http': 'http://ironport2.iitk.ac.in:3128',
	'https': 'http://ironport2.iitk.ac.in:3128',
}
tr_num = []
tr_runtime = {}
for u in url:
	r = requests.get(u, proxies=proxies)
	soup = BeautifulSoup(r.text, "lxml")
	table = soup.find_all(class_='TrainsList table table-bordered table-condensed ')
	table = table[0]
	for i in range(0,len(table('tr')[1:])):
		if i%3 != 1:
			continue
		# print(i)
		t = table('tr')[i]
		p = t.find_all('td')
		tr_num.append(p[2].text)
		tr_runtime[(p[2].text)] = (p[4].text)

# Remove duplicates
list(set(tr_num))

#store data in cnb_ndls
fd = open('cnb_ndls', 'w')
for i in range(len(tr_num)):
	to_write=tr_num[i]+","+tr_runtime[tr_num[i]]+"\n"
	fd.write(to_write)
fd.close()

print("Train list extracted")

for num in tr_num:
	url = 'http://www.irctclive.in/RunningTrainHistoryStatus/'+str(num)+'/lastyear'
	print("Looking at train " + str(num))
	r = requests.get(url)
	soup = BeautifulSoup(r.text, 'lxml')
	if 'Server Error (500)' in soup.text:
		continue
	table = soup.find_all(class_='table datatable table-striped table-bordered')
	ofname = 'Data/cnb_ndls/'+str(num)
	of = open(ofname, 'w')
	for row in table[0]('tr')[1:]:
		col = row('td')
		# date.append(col[0].string)
		# delay.append(col[2].string)
		delay_hrs = str(col[2].string)
		if delay_hrs == '-':
			delay = 0
		else:
			split_delay = delay_hrs.split(' ')
			split_delay = [d for d in split_delay if d != '']
			if 'Hr' in split_delay[1]:
				delay = int(split_delay[0])*60
				if 'Min' in split_delay[3]:
					delay = delay + int(split_delay[2])
			else:
				if 'Min' in split_delay[1]:
					delay = int(split_delay[0])
		writestr=str(col[0].string)+','+str(delay)+' Mins\n'
		of.write(writestr)
	of.close()