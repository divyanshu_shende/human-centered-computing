from bs4 import BeautifulSoup
import requests
import pandas as pd
import os
from scipy import stats
import numpy as np

# CNB to NDLS trains data
total_delay_cnb_ndls = {}
mean_delays_cnb_ndls = {}
days_cnb_ndls = {}
path = 'Data/cnb_ndls/'
files = os.listdir(path)
for train in files:
	fpath = path + str(train)
	# print(train)
	fd = open(fpath, 'r')
	data = fd.read()
	data = data.split('\n')
	data = [x for x in data if x != '']
	fd.close()
	delay = []
	for line in data:
		stuff = line.split(',')
		# print(stuff)
		delay_mins = stuff[-1]
		if delay_mins.endswith(' Mins') == True:
			delay_mins = delay_mins[:-5]
			# print(delay_mins)
			delay.append(int(delay_mins))
	mean_delays_cnb_ndls[train] = (sum(delay)/len(delay))
	days_cnb_ndls[train] = len(delay)

# Get the runtimes!
runtimes_cnb_ndls = {}
runtime_path = 'cnb_ndls'
runf = open(runtime_path, 'r')
dat = runf.read()
dat = dat.split('\n')
dat = [x for x in dat if x != '']
runf.close()
for entry in dat:
	p = entry.split(',')
	rt_hrs = (p[1].split(':'))[0]
	rt_min = (p[1].split(':'))[1]
	rt_hrs = rt_hrs.strip('\n')
	rt_min = rt_min.strip('\n')
	rt = int(rt_hrs)*60 + int(rt_min)
	tnum = p[0].strip('\n')
	runtimes_cnb_ndls[tnum] = rt
	total_delay_cnb_ndls[tnum] = rt + mean_delays_cnb_ndls[tnum]


d = sorted(total_delay_cnb_ndls.items(), key=lambda x: x[1])
print("The top 5 trains from CNB to NDLS with mean delays are (min 50 days): ")
print("Tr.No\tDays\t\tRuntime\t\tDelay\t\tTot\n")
i, count = 0, 0
while count<5:
	# Discount special trains that ran for a week or so. Can be set higher.
	if days_cnb_ndls[d[i][0]] < 50:
		i = i + 1
		continue
	to_write = str(d[i][0]) + "\t" + str(days_cnb_ndls[d[i][0]])+ "\t\t" + str(round(runtimes_cnb_ndls[d[i][0]], 3)) +\
	 "\t\t" + str(round(mean_delays_cnb_ndls[d[i][0]], 3)) + "\t\t" + str(round(d[i][1], 3))
	count = count + 1
	i = i + 1
	print(to_write)


# CSTM to Pune trains data
total_delay_cstm_pune = {}
mean_delays_cstm_pune = {}
days_cstm_pune = {}
path = 'Data/cstm_to_pune/'
files = os.listdir(path)
for train in files:
	fpath = path + str(train)
	# print(train)
	fd = open(fpath, 'r')
	data = fd.read()
	data = data.split('\n')
	data = [x for x in data if x != '']
	fd.close()
	delay = []
	for line in data:
		stuff = line.split(',')
		# print(stuff)
		delay_mins = stuff[-1]
		if delay_mins.endswith(' Mins') == True:
			delay_mins = delay_mins[:-5]
			# print(delay_mins)
			delay.append(int(delay_mins))
	mean_delays_cstm_pune[train] = (sum(delay)/len(delay))
	days_cstm_pune[train] = len(delay)

# Get the runtimes!
runtimes_cstm_pune = {}
runtime_path = 'cstm_to_pune'
runf = open(runtime_path, 'r')
dat = runf.read()
dat = dat.split('\n')
dat = [x for x in dat if x != '']
runf.close()
for entry in dat:
	p = entry.split(',')
	rt_hrs = (p[1].split(':'))[0]
	rt_min = (p[1].split(':'))[1]
	rt_hrs = rt_hrs.strip('\n')
	rt_min = rt_min.strip('\n')
	rt = int(rt_hrs)*60 + int(rt_min)
	tnum = p[0].strip('\n')
	runtimes_cstm_pune[tnum] = rt
	total_delay_cstm_pune[tnum] = rt + mean_delays_cstm_pune[tnum]

d = sorted(total_delay_cstm_pune.items(), key=lambda x: x[1])
print("\n\nThe top 5 trains from CSTM to PUNE with mean delays are (min 100 days): ")
print("Tr.No\tDays\t\tRuntime\t\tDelay\t\tTot\n")
i, count=0, 0
while count<5:
	# Discount special trains that ran for a week or so. Can be set higher.
	if days_cstm_pune[d[i][0]] < 100:
		i = i + 1
		continue
	to_write = str(d[i][0]) + "\t" + str(days_cstm_pune[d[i][0]])+ "\t\t" + str(round(runtimes_cstm_pune[d[i][0]], 3)) +\
	 "\t\t" + str(round(mean_delays_cstm_pune[d[i][0]], 3)) + "\t\t" + str(round(d[i][1], 3))
	count = count + 1
	i = i + 1
	print(to_write)