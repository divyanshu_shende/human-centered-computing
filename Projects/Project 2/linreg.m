function linreg (x, y)

plot(x,y,'rx'); % Plot the data
hold on;
m = length(x);
% Add a column of all ones (intercept term) to x
X = [ones(m, 1) x];
theta = (pinv(X'*X))*X'*y
hold on; % this keeps our previous plot of the training data visible
plot(X(:,2), X*theta, '-');
hold off;

end