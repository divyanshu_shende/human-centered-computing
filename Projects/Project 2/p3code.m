mlgrad = 0;
mlstaff = 0;
prof = 0;
firstgrad = 0;
sloan = 0;

comm = ones(106,1);
aff = ones(106,1);
comm_score = zeros(8,1);
ex2 = zeros(8,1);
var_cs = zeros(8,1);
len = zeros(8,1);


for i = 1:106
a =  s(i).my_affil;
b = s(i).my_community;

c = char(a);
d = char(b);

if(strcmp(c,'mlgrad')==1) mlgrad = mlgrad+1; aff(i) = 2;
end
if(strcmp(c,'mlstaff')==1) mlstaff = mlstaff+1; aff(i) = 3;
end
if(strcmp(c,'professor')==1) prof = prof+1; aff(i) = 4;
end
if(strcmp(c,'1styeargrad ')==1) firstgrad = firstgrad+1; aff(i) = 5;
end
if(strcmp(c,'sloan')==1) sloan = sloan+1; aff(i) = 6;
end
if(strcmp(c,'mlurop')==1) sloan = sloan+1; aff(i) = 7;
end
if(strcmp(c,'mlfrosh')==1) sloan = sloan+1; aff(i) = 8;
end

%1 means info unavailable
if(strcmp(d,'very close')==1) comm(i) = 3;
end
if(strcmp(d,'somewhat close')||strcmp(d,'somewhat')|| strcmp(d,'a little close')) comm(i) = 2;
end

af = aff(i);
comm_score(af) = (comm_score(af)*len(af))+comm(i);
ex2(af) = ex2(af)+comm(i)^2;
len(af) = len(af)+1;
comm_score(af) = (comm_score(af))/len(af);
end

for af = 1:8
ex2(af) = ex2(af)/len(af);
var_cs(af) = ex2(af) - comm_score(af)^2 ;
end

f = network.friends;
ind_list = network.sub_sort;
f_num = -1*ones(106,1);

for i = 1:94
    f_num(ind_list(i)) = 94 - sum(isnan(f(i,:)));
end

%total_friends = sum(f_num)

%find correlation between no. of friends and community score
% 
% %mean of no. of friends:
% sum_f = sum(f_num);
% hold_var = sum(f_num==-1);
% sum_f = sum_f - hold_var;
% mu_f = sum_f/(106+hold_var);
% 
% %mean of community score
% sum_comm = sum(comm);
% hold_var = sum(comm==1);
% sum_comm = sum_comm - hold_var;
% mu_comm = sum_comm/(106+hold_var);

sum_f = 0;
sum_comm = 0;
n = 0; 
exy = 0;
x2 = 0; y2 = 0;
f_aff = zeros(8,1);
%corr(x,y) = E(XY) - E(X)E(Y);
for i = 1:106
    if(f_num(i)~=-1 && comm(i)~=1)
        sum_f = sum_f+f_num(i);
        sum_comm = sum_comm + comm(i);
        n = n+1;
        exy = exy + f_num(i)*comm(i);
        x2 = x2+f_num(i)^2;
        y2 = y2 + comm(i)^2;
        
        f_aff(aff(i)) = f_num(i) + f_aff(aff(i));
  
    end
end
exy = exy/n;
mu_f = sum_f/n
mu_comm = sum_comm/n
var_f = x2/n
var_comm = y2/n
corr_fcomm = (exy - mu_f*mu_comm)/sqrt(var_f + var_comm) %correlation between comm_Score and no. of friends

%call logs
out = zeros(106,1);
in = zeros(106,1);
totalcalls = zeros(106,1);
for j = 1:106
    %out(j) = 0; in(j) = 0;
    hold_var = s(j).comm;
   %call(j) = hold_var;
   for i = 1:size(hold_var, 2)
       totalcalls(j) = size(hold_var,2);
   if(strcmp(hold_var(i).direction, 'Outgoing')==1) out(j) = out(j)+1;
   else in(j) = in(j)+1;
       
   end
   end
   out(j) = out(j)/totalcalls(j);
   in(j) = in(j)/totalcalls(j);
   
end
    
     %XX = {'computer', 'car', 'computer', 'bus', 'tree', 'car'}
%     [uniquedir, ~, J]=unique(dir) 
%     occ = histc(J, 1:numel(uniquedir))
%     call_in(i) = occ(1);
%     call_out(i) = occ(2);
%     


%corr between incoming and outgoing calls and community score and
%corr(x,y) = E(XY) - E(X)E(Y);
sum_out = 0; sum_in = 0;
sum_comm1 = 0;
sum_calls = 0; sum_f = 0;
n1 = 0; 
exy1 = 0; exy2 = 0; exy3 = 0; exy4 = 0;
x2 = 0; y2 = 0; 
x3 = 0; x4 = 0; x5 = 0;

home = zeros(106,1);
work = zeros(106,1);
elsewhere = zeros(106,1);
nosig = zeros(106,1);

for i = 1:106
    kj = s(i).data_mat;
    home(i) = sum(sum(kj(:,:)==1));
    work(i) = sum(sum(kj(:,:)==2));
    elsewhere(i) = sum(sum(kj(:,:)==3));
    nosig(i) = sum(sum(kj(:,:)==0));
    
    if(isnan(out(i))==0 && comm(i)~=1 && f_num(i)~=-1)
        sum_out = sum_out+out(i);
        sum_in = sum_in+in(i);
        sum_calls = sum_calls+totalcalls(i);
        sum_comm1 = sum_comm1 + comm(i);
        sum_f = sum_f + f_num(i);
        n1 = n1+1;
        exy1 = exy1 + out(i)*comm(i);
        exy2= exy2 + in(i)*comm(i);
        exy3 = exy3 + totalcalls(i)*comm(i);
        exy4 = exy4 + f_num(i)*totalcalls(i);
       
        x2 = x2+out(i)^2;
        x3 = x3+in(i)^2;
        x4 = x4 + totalcalls(i)^2;
        x5 = x5 + f_num(i)^2;
        y2 = y2 + comm(i)^2;
        
        %f_aff(aff(i)) = f_num(i) + f_aff(aff(i));
  
    end
end
exy1 = exy1/n1;
exy2 = exy2/n1;
exy3 = exy3/n1;
exy4 = exy4/n1;
mu_out = sum_out/n1
mu_in = sum_in/n1
mu_calls = sum_calls/n1
mu_comm1 = sum_comm1/n1
mu_f = sum_f/n1
var_out = x2/n1
var_in = x3/n1
var_calls = x4/n1
var_comm1 = y2/n1
var_f = x5/n1
corr_outcomm = (exy1 - mu_out*mu_comm1)/sqrt(var_out + var_comm1) %correlation between comm_Score and no. of friends
corr_incomm = (exy2 - mu_in*mu_comm1)/sqrt(var_in + var_comm1)
corr_callcomm = (exy3 - mu_calls*mu_comm1)/sqrt(var_calls + var_comm1)
corr_callf = (exy4 - mu_calls*mu_f)/sqrt(var_calls + var_f)

%scatter(home, comm)
%hold;
%scatter(work, comm,'r')
%hold;
%scatter(elsewhere, comm, 'g')
%xlabel('hours spent')
%ylabel('community score')
%legend('blue: home','red: work', 'green: elsewhere')
%hold;

%% Linear regression function
function linreg (x, y)

plot(x,y,'rx'); % Plot the data
hold on;
m = length(x);
X = [ones(m, 1) x]; % Intercept term
theta = (pinv(X'*X))*X'*y
hold on;
plot(X(:,2), X*theta, '-'); % Plot the line
hold off;

end

function [ personid ] = mapmac(maclist, macid)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
personid = -1;
for i = 1:104
    if(maclist(i)==macid)
        personid = i;
        break;
    end

end
end


for i=1:104
maclist(i) = s(i).mac;
end

%printf("Starting proximity calculation\n")
%proximity_mat = zeros(104, 104);
%for i=1:104
%  i
%  x = s(i).device_date;
%  if(length(x)==0)
%    continue;
%  end
%  y = s(i).device_macs;
%  p = find(x==0);
%  x(p) = [];
%  y(p) = [];
%  q = find(weekday(datestr(x))~=7 && weekday(datestr(x))~=1);
%  x(q) = [];
%  y(q) = [];
%  for j=1:length(y)
%    for k=1:length(y{j})
%       person_id = find(maclist == y{j}(k));
%       if(length(person_id) ~= 0)
%        proximity_mat(i, person_id) = proximity_mat(i, person_id) + 1;
%       end
%     end
%  end
%end

% Use sum(proximity_mat(:, :)') to get the total number of saturday interactions

f_mat = zeros(104, 104);
for i=1:104
  for j=1:104
  t_i = find(ind_list == i);
  t_j = find(ind_list == j);
    if(length(t_i)~=0 && length(t_j)~=0)
    f_mat(i, j) = ~isnan(f(t_i, t_j));
    end
  end
end

% introvertness
introvert = -1*ones(106, 1);
for i=1:106
response = s(i).my_intros;
if(length(response) == 0) continue; end
response = s(i).my_intros{1};
if(length(response) < 2) continue; end 
g = response(1:2);
if(strcmp(g, 'ne')) introvert(i) = 0; end
if(strcmp(g, 'ra') || strcmp(g, 'Ra')) introvert(i) = 1; end
if(strcmp(g, 'oc') || strcmp(g, 'Oc') || strcmp(g, 'so')) introvert(i) = 2; end
if(strcmp(g, 'of') || strcmp(g, 'Of')) introvert(i) = 3; end
end


% friends circle evolution
f_circle = -1*ones(106, 1);
for i=1:106
  response = s(i).surveydata;
  if(length(response) == 0) continue; end
  response = response(5);
  if(isnan(response)) f_circle(i) = -1; continue; end
  f_circle(i) = 4-response;
end