mlgrad = 0;
mlstaff = 0;
prof = 0;
firstgrad = 0;
sloan = 0;

comm = ones(106,1);
aff = ones(106,1);
comm_score = zeros(8,1);
ex2 = zeros(8,1);
var_cs = zeros(8,1);
len = zeros(8,1);

for i = 1:106
a =  s(i).my_affil;
b = s(i).my_community;

c = char(a);
d = char(b);

if(strcmp(c,'mlgrad')==1) mlgrad = mlgrad+1; aff(i) = 2;
end
if(strcmp(c,'mlstaff')==1) mlstaff = mlstaff+1; aff(i) = 3;
end
if(strcmp(c,'professor')==1) prof = prof+1; aff(i) = 4;
end
if(strcmp(c,'1styeargrad ')==1) firstgrad = firstgrad+1; aff(i) = 5;
end
if(strcmp(c,'sloan')==1) sloan = sloan+1; aff(i) = 6;
end
if(strcmp(c,'mlurop')==1) mluprop = mluprop+1; aff(i) = 7;
end
if(strcmp(c,'mlfrosh')==1) sloan = mlfrosh+1; aff(i) = 8;
end

%1 means info unavailable
if(strcmp(d,'very close')==1) comm(i) = 3;
end
if(strcmp(d,'somewhat close')||strcmp(d,'somewhat')|| strcmp(d,'a little close')) comm(i) = 2;
end

comaf = zeros(8,1);
countaf = zeros(8,1);
for i = 1:106
    if(comm(i)~=1) comaf(aff(i)) = comaf(aff(i)) + comm(i);
        countaf(aff(i)) = countaf(aff(i))+1;
    end
end
comaf = comaf./countaf;

af = aff(i);
comm_score(af) = (comm_score(af)*len(af))+comm(i);
ex2(af) = ex2(af)+comm(i)^2;
len(af) = len(af)+1;
comm_score(af) = (comm_score(af))/len(af);
end

for af = 1:8
ex2(af) = ex2(af)/len(af);
var_cs(af) = ex2(af) - comm_score(af)^2 ;
end

f = network.friends;
ind_list = network.sub_sort;
f_num = -1*ones(106,1);

for i = 1:94
    f_num(ind_list(i)-1) = 94 - sum(isnan(f(i,:)));
end

reciprocal = [];
non_rec = [];
nonf = [];

for i = 1:94
    for j = 1:94
if(f(i,j)==1)
    if(f(j,i)==1 && i<j) reciprocal = [reciprocal; ind_list(i); ind_list(j)];
    else if(i~=j) non_rec = [non_rec; ind_list(i); ind_list(j)];
    end
    end
else nonf = [nonf; ind_list(i); ind_list(j)];
    end
    end
end


 r1 = reciprocal(1:2:56);
 r2 = reciprocal(2:2:56);
 nr1 = non_rec(1:2:164);
 nr2 = non_rec(2:2:164);
 l = length(nonf);
 nf1 = nonf(1:2:l);
 nf2 = nonf(2:2:l);
 
 rec_matrix = zeros(106,106);
 nonrec_matrix = zeros(106,106);   
 
 
 for i = 1:28
 rec_matrix(r1(i), r2(i)) = 1;
 end
 for i = 1:82
 nonrec_matrix(nr1(i), nr2(i)) = 1;
 end
 rec_vector = reshape(rec_matrix(:,1:106)',1,106*106);
 
groupcalls = zeros(106,1);
calls = zeros(106,106);
for iter = 1:106
 for i = 1:size(s(iter).comm, 2)
aa = s(iter).comm;
bb = aa(i).hashNum;
if(bb<106)
groupcalls(iter) = groupcalls(iter) + 1;
calls(iter,bb) = calls(iter, bb) + 1;
end
 end
end

calls_vector = reshape(calls', 1, 106*106);
corr_callsrec = corrcoef(calls_vector, rec_vector);
corr_callsnrec = corrcoef(calls, nonrec_matrix(1:106,1:106));

%total_friends = sum(f_num)

%find correlation between no. of friends and community score
% 
% %mean of no. of friends:
% sum_f = sum(f_num);
% hold = sum(f_num==-1);
% sum_f = sum_f - hold;
% mu_f = sum_f/(106+hold);
% 
% %mean of community score
% sum_comm = sum(comm);
% hold = sum(comm==1);
% sum_comm = sum_comm - hold;
% mu_comm = sum_comm/(106+hold);

sum_f = 0;
sum_comm = 0;
n = 0; 
exy = 0;
x2 = 0; y2 = 0;
f_aff = zeros(8,1);
%corr(x,y) = E(XY) - E(X)E(Y);
for i = 1:106
    if(f_num(i)~=-1 && comm(i)~=1)
        sum_f = sum_f+f_num(i);
        sum_comm = sum_comm + comm(i);
        n = n+1;
        exy = exy + f_num(i)*comm(i);
        x2 = x2+f_num(i)^2;
        y2 = y2 + comm(i)^2;
        
        f_aff(aff(i)) = f_num(i) + f_aff(aff(i));
  
    end
end
exy = exy/n;
mu_f = sum_f/n
mu_comm = sum_comm/n
var_f = x2/n
var_comm = y2/n
corr_fcomm = (exy - mu_f*mu_comm)/sqrt(var_f + var_comm) %correlation between comm_Score and no. of friends

%%call logs
out = zeros(106,1);
in = zeros(106,1);
totalcalls = zeros(106,1);
for j = 1:106
    %out(j) = 0; in(j) = 0;
    hold1 = s(j).comm;
   %call(j) = hold;
   for i = 1:size(hold1, 2)
       totalcalls(j) = size(hold1,2);
   if(strcmp(hold1(i).direction, 'Outgoing')==1) out(j) = out(j)+1;
   else in(j) = in(j)+1;
       
   end
   end
   out(j) = out(j)/totalcalls(j);
   in(j) = in(j)/totalcalls(j);
   
end

%%bluetooth proximity

prox = zeros(106,106);

for i = 1:104
maclist(i) = s(i).mac;
end


for i = 1:106
    
    blue = s(i).device_macs;
    nscans = size(blue, 2);
    
    for j = 1:nscans
        for k = 1:size(blue{j})
            bindex = mapmac(maclist, blue{j}(k));
            if(bindex~=-1)
                prox(i,bindex) = prox(i,bindex)+1;
            end
        end
    end
end

   %%normalize proximity and total calls matrices, and sum them. Find the correlation with the frienship matrix. 
  ncalls = zeros(106,106);
  nprox = zeros(106,106);
  for i = 1:106
       if(sum(calls(i,:))>0)ncalls(i,:) = calls(i,:)/sum(calls(i,:)); end
       if(sum(prox(i,:)>0))nprox(i,:) = prox(i,:)/sum(prox(i,:)); end
    %   ncalls(i,107) = sum(calls(i,:));
    %   nprox(i,107) = sum(prox(i,:));
  end
   
     ncalls_prox = ncalls + nprox;
     corrcoef(ncalls_prox, rec_matrix(1:106,1:106))
     corrcoef(ncalls_prox, nonrec_matrix(1:106,1:106))
    
%corr between incoming and outgoing calls and community score and
%corr(x,y) = E(XY) - E(X)E(Y);
sum_out = 0; sum_in = 0;
sum_comm1 = 0;
sum_calls = 0; sum_f = 0;
n1 = 0; 
exy1 = 0; exy2 = 0; exy3 = 0; exy4 = 0;
x2 = 0; y2 = 0; 
x3 = 0; x4 = 0; x5 = 0;

home = zeros(106,1);
work = zeros(106,1);
elsewhere = zeros(106,1);
nosig = zeros(106,1);

for i = 1:106
    kj = s(i).data_mat;
    home(i) = sum(sum(kj(:,:)==1));
    work(i) = sum(sum(kj(:,:)==2));
    elsewhere(i) = sum(sum(kj(:,:)==3));
    nosig(i) = sum(sum(kj(:,:)==0));
    
    if(isnan(out(i))==0 && comm(i)~=1 && f_num(i)~=-1)
        sum_out = sum_out+out(i);
        sum_in = sum_in+in(i);
        sum_calls = sum_calls+totalcalls(i);
        sum_comm1 = sum_comm1 + comm(i);
        sum_f = sum_f + f_num(i);
        n1 = n1+1;
        exy1 = exy1 + out(i)*comm(i);
        exy2= exy2 + in(i)*comm(i);
        exy3 = exy3 + totalcalls(i)*comm(i);
        exy4 = exy4 + f_num(i)*totalcalls(i);
       
        x2 = x2+out(i)^2;
        x3 = x3+in(i)^2;
        x4 = x4 + totalcalls(i)^2;
        x5 = x5 + f_num(i)^2;
        y2 = y2 + comm(i)^2;
        
        %f_aff(aff(i)) = f_num(i) + f_aff(aff(i));
  
    end
end
exy1 = exy1/n1;
exy2 = exy2/n1;
exy3 = exy3/n1;
exy4 = exy4/n1;
mu_out = sum_out/n1
mu_in = sum_in/n1
mu_calls = sum_calls/n1
mu_comm1 = sum_comm1/n1
mu_f = sum_f/n1
var_out = x2/n1
var_in = x3/n1
var_calls = x4/n1
var_comm1 = y2/n1
var_f = x5/n1
corr_outcomm = (exy1 - mu_out*mu_comm1)/sqrt(var_out + var_comm1) %correlation between comm_Score and no. of friends
corr_incomm = (exy2 - mu_in*mu_comm1)/sqrt(var_in + var_comm1)
corr_callcomm = (exy3 - mu_calls*mu_comm1)/sqrt(var_calls + var_comm1)
corr_callf = (exy4 - mu_calls*mu_f)/sqrt(var_calls + var_f)

scatter(home, comm)
hold on;
scatter(work, comm,'r')
hold on
scatter(elsewhere, comm, 'g')
xlabel('hours spent')
ylabel('community score')
legend('blue: home','red: work', 'green: elsewhere')


for i  = 1:106
if(size(s(i).surveydata>0))
rish(i) = s(i).surveydata(5);
else
rish(i)= -1;
end
end

j = 1;
for i = 1:106
    if(rish(i)~=-1 && isnan(rish(i))==0)
        x(j) = rish(i); y(j) = elsewhere(i);
        j = j+1;
    end
end

r = corrcoef(x,y);


for i = 1:106
    if(strcmp(s(i).my_provider, 'T-mobile')==1) serv(i) = 1;
    else if(strcmp(s(i).my_provider, 'cingular')==1) serv(i) = 2;
        else if (strcmp(s(i).my_provider, 'verizon')==1) serv(i) =3 ;
            else if(strcmp(s(i).my_provider, 'AT&T')==1) serv(i) = 4;
                    else if(strcmp(s(i).my_provider, 'ATT Wireless')==1) serv(i) = 5;
                        else serv(i) = 6;
                end
            end
        end
    end
end
end



ss= 0;ss2 = 0;
for i = 1:106
if(serv(i)==2)
ss = ss+nosig(i);
ss2 = ss2 + nosig(i)^2;
end
end
sqrt(ss2/6 - (ss/6)^2)

for i = 1:106
    if(size(s(i).surveydata,1)>0)
    sat(i) = s(i).surveydata(22);
    else sat(i) = -1;
    end
end

j = 1;
for i = 1:106
    if(sat(i)~=-1 && isnan(sat(i))==0 && f_num(i)~=-1)
        x(j) = sat(i); y(j) = f_num(i);
        j = j+1;
    end
end

r = corrcoef(x,y);

plot(ncalls(40,:),'b')
hold on;
plot(nprox(40,:),'r')
legend('blue: normalizd calls', 'red: normalized proxmitiy');
title('for subject 40')


%%predicting if a pair of people are friends or not -- reciprocal or non
%%reciprocal both

%%feature matrix -- no. of calls, affilition, proximity, closeness to
%%campus

%first 20 of reciprocal friends and first 60 of reciprocal friends for
%training
    for i = 1:20
        if(r1(i)<107 && r2(i)<107)
        X(i,1) = ncalls(r1(i),r2(i));
        X(i,2) = aff(r1(i));
        X(i,3) = aff(r2(i));
        X(i,4) = nprox(r1(i), r2(i));
        Y(i) = 1;
        else i = i-1;
        %X(i,5) =
        end
    end
    
    for i = 21:80
        if(nr1(i-20)<107&&nr2(i-20)<107)
        X(i,1) = ncalls(nr1(i-20),nr2(i-20));
        X(i,2) = aff(nr1(i-20));
        X(i,3) = aff(nr2(i-20));
        X(i,4) = nprox(nr1(i-20), nr2(i-20));
        Y(i) = 1;
        else
            i = i-1;
        end
    end
    

    %using 150 non friends also in training set
    for i = 81:230
        if(nf1(i-80)<107&&nf2(i-80)<107)
        X(i,1) = ncalls(nf1(i-80),nf2(i-80));
    X(i,2) = aff(nf1(i-80));
    X(i,3) = aff(nf2(i-80));
    X(i,4) = nprox(nf1(i-80), nf2(i-80));
    Y(i) = 0;
        else i = i-1;
        end
end

%8 reciprocal, 22 non reciprocal and 50 non friends in test set
for i = 1:7
    if(r1(i+20)<107 && r2(i+20)<107)
    X_test(i,1) = ncalls(r1(i+20),r2(i+20));
    X_test(i,2) = aff(r1(i+20));
    X_test(i,3) = aff(r2(i+20));
    X_test(i,4) = nprox(r1(i+20), r2(i+20));
    Y_test(i) = 1;
    else i = i-1;
    end
    %X(i,5) =
end
for i = 8:29
    if(nr1(i+53)<107 && nr2(i+53)<107)
    X_test(i,1) = ncalls(nr1(i+53),nr2(i+53));
    X_test(i,2) = aff(nr1(i+53));
    X_test(i,3) = aff(nr2(i+53));
    X_test(i,4) = nprox(nr1(i+53), nr2(i+53));
    Y_test(i) = 1;
    else i = i-1;
    end
   
    %X(i,5) =
end
for i = 30:79
    if(nf1(i+121)<107 && nf2(i+121)<107)
    X_test(i,1) = ncalls(nf1(i+121),nf2(i+121));
    X_test(i,2) = aff(nf1(i+121));
    X_test(i,3) = aff(nf2(i+121));
    X_test(i,4) = nprox(nf1(i+121), nf2(i+121));
    Y_test(i) = 0;
    %X(i,5) =
end



X(:,2) = X(:,2)/6;
X(:,3) = X(:,3)/6;

onevec = ones(230,1);
Xaug = [X onevec];
b = regress(Y', Xaug);
Ypred = Xaug*b;

%X_test(:,2) = X_test(:,2)/6;
%X_test(:,3) = X_test(:,3)/6;
%onevectest = ones(79,1);
%Xtestaug = [X_test onevectest];
%Ypredtest = Xtestaug*b;

